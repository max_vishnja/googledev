@extends(config('google.views.layout'))

@section('content')
    <div class="row-fluid">
        <!-- block -->
        <div class="panel panel-default">
            <div class="navbar navbar-default">
                <div class="panel-heading">Google analytics accounts</div>

            </div>

            <div class="panel-body">
                @if(isset($error))
                    <div class="alert alert-dismissable alert-danger">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <ul class="list-group">
                            <li class="list-group-item">{{ $error }}</li>
                        </ul>
                    </div>
                @else
                    @if(isset($accounts) and !empty($accounts))
                        <div class="panel-group" id="accounts">
                            <h2 class="text-center">We greet you! Select your account on the left !</h2>
                        </div>
                    @else
                        <div class="panel-group" id="accounts">
                            <h2 class="text-center">Sorry!!! You account in Google Analytics is empty</h2>
                        </div>
                    @endif
                @endif


            </div>
        </div>
    </div>
    <!-- /block -->
    </div>

@stop