<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Admin Home Page</title>
    <!-- Bootstrap -->
    <link href="/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/public/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="/public/vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
    <link href="/public/assets/styles.css" rel="stylesheet" media="screen">
    <script src="/public/vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>