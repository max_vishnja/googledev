<?php

namespace MaxVishnja\GoogleAnalytics\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;
use MaxVishnja\GoogleAnalytics\Http\Requests\GoogleAnalytics;

class GoogleAnalyticsDataController extends Controller
{

    protected $analytics;

    public function __construct(App $analytics)
    {
        $this->analytics = App::make('analytics');
        $this->middleware('auth');
    }

    /**
     * Show List accounts
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $analytics = $this->analytics;
        try {
            $accounts = $analytics->management_accounts->listManagementAccounts();
        } catch (apiServiceException $e) {
            return view('googleanalytics::google/analytics.index')
                ->with('error', 'There was an Analytics API service error ' . $e->getCode() . ':' . $e->getMessage());
        } catch (apiException $e) {
            return view('googleanalytics::google/analytics.index')
                ->with('error', 'There was a general API error ' . $e->getCode() . ':' . $e->getMessage());
        } catch (\Exception $e) {
            return view('googleanalytics::google/analytics.index')
                ->with('error', 'There was a general API error ' . $e->getCode() . ': ' . $e->getMessage());
        }

        return view('googleanalytics::google/analytics.index', ['accounts' => $accounts->getItems()]);

    }

    /**
     * Get web property for one account (ajax request)
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAccount($id)
    {
        $analytics = $this->analytics;
        $webProperties = $analytics->management_webproperties->listManagementWebproperties($id);
        return view('googleanalytics::google/analytics.account', ['webProperties' => $webProperties->getItems()]);
    }

    /**
     * Show information web on site
     *
     * @param $idAccount
     * @param $idProperty
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getWebProperty($idAccount, $idProperty)
    {
        $analytics = $this->analytics;
        $accounts = $analytics->management_accounts->listManagementAccounts();
        $profiles = $analytics->management_profiles
            ->listManagementProfiles($idAccount, $idProperty);
        $profiles = $profiles->getItems();
        $endDate = Carbon::now()->toDateString();
        $startDate = Carbon::now()->subDays(30)->toDateString();
        $data['SstartDate'] = $startDate;
        $data['EendDate'] = $endDate;
        $data['endDate'] = Carbon::now()->toFormattedDateString();
        $data['startDate'] = Carbon::now()->subDays(30)->toFormattedDateString();
        $visitByDay = $this->getGeneralStatistics($analytics, $profiles[0]->id, $startDate, $endDate);
        $arr = array('ga:sessions', 'ga:pageviews', 'ga:users', 'ga:newUsers', 'ga:avgSessionDuration', 'ga:bounceRate', 'ga:percentNewSessions');
        foreach ($arr as $val) {
            $data[$val] = $this->getLittleGraph($profiles[0]->id, $startDate, $endDate, $val);
        }
        return view('googleanalytics::google/analytics.profile',
            ['profile' => $profiles[0],
                'accounts' => $accounts->getItems(),
                'visit' => $visitByDay,
                'data' => $data
            ]);
    }


    /**
     * Show statistic for web property
     * Call AJAX
     *
     * @param GoogleAnalytics $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatistic(GoogleAnalytics $request, $id)

    {
        $analytics = $this->analytics;
        $visitByDay = $this->getVisitByDay($analytics, $id, $request->startDate, $request->endDate);
        $visitBy7Day = $this->getVisitBy7Day($analytics, $id, $request->startDate, $request->endDate);
        $visitBy30Day = $this->getVisitBy30Day($analytics, $id, $request->startDate, $request->endDate);
        $generalStatistics = $this->getGeneralStatistics($analytics, $id, $request->startDate, $request->endDate);
        return response()->json(array(
            'visitByDay' => $visitByDay,
            'visitBy7Day' => $visitBy7Day,
            'visitBy30Day' => $visitBy30Day,
            'generalStatistics' => $generalStatistics));
    }


    /**
     * Get statistic visits and pageviews by day for the selected period
     *
     * @param $analytics
     * @param $id
     * @param $startDate
     * @param $endDate
     * @return \Illuminate\Http\JsonResponse
     */

    private function getVisitByDay($analytics, $id, $startDate, $endDate)
    {
        $results = $analytics->data_ga->get(
            'ga:' . $id,
            $startDate,
            $endDate,
            'ga:sessions',
            array('dimensions' => 'ga:date')
        );

        $response = [];
        foreach ($results->getRows() as $key => $row) {
            $response[$key] = array((date("Y-m-d", strtotime($row[0]))), ((int)$row[1]));
        }

        return $response;
    }

    /**
     * Get statistic visits and pageviews by 7day for the selected period
     *
     * @param $analytics
     * @param $id
     * @param $startDate
     * @param $endDate
     * @return \Illuminate\Http\JsonResponse
     */

    private function getVisitBy7Day($analytics, $id, $startDate, $endDate)
    {
        $results = $analytics->data_ga->get(
            'ga:' . $id,
            $startDate,
            $endDate,
            'ga:7dayUsers',
            array('dimensions' => 'ga:date')
        );

        $response = [];
        foreach ($results->getRows() as $key => $row) {
            $response[$key] = array((date("Y-m-d", strtotime($row[0]))), ((int)$row[1]));
        }

        return $response;
    }

    /**
     * Get statistic visit by 7day or 30day for the selected period
     * @param $analytics
     * @param $id
     * @param $startDate
     * @param $endDate
     * @return array
     */
    private function getVisitBy30Day($analytics, $id, $startDate, $endDate)
    {
        $results = $analytics->data_ga->get(
            'ga:' . $id,
            $startDate,
            $endDate,
            'ga:30dayUsers',
            array('dimensions' => 'ga:date')
        );

        $response = [];
        foreach ($results->getRows() as $key => $row) {
            $response[$key] = array((date("Y-m-d", strtotime($row[0]))), ((int)$row[1]));
        }

        return $response;
    }

    /**
     * Show map for web property
     * Call AJAX
     * @param id , startdate, enddate, metrics
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMap()
    {
        $analytics = $this->analytics;
        if (Request::ajax()) {
            $id = Input::get('id');
            $metrics = Input::get('param');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
        }
        $results = $analytics->data_ga->get(
            'ga:' . $id,
            $startDate,
            $endDate,
            $metrics,
            array('dimensions' => 'ga:countryIsoCode')
        );

        $response = [];
        foreach ($results->getRows() as $key => $row) {
            $response[$key] = array((strtolower($row[0])) => ((int)$row[1]));
        }
        $resp = [];
        foreach ($response as $key => $row) {
            $resp = array_merge($resp, $response[$key]);
        }
        return json_encode($resp);

    }


    /**
     * Get Small Graph or big graph of statistic
     * Call Ajax and from route
     * @param null $id
     * @param null $startDate
     * @param null $endDate
     * @param null $param
     * @param null $big
     * @return array|string
     */
    public function getLittleGraph($id = null, $startDate = null, $endDate = null, $param = null, $big = null)
    {
        $analytics = $this->analytics;
        if (Request::ajax()) {
            $id = Input::get('id');
            $param = Input::get('param');
            $startDate = Input::get('startDate');
            $endDate = Input::get('endDate');
            $big = Input::get('big');
        }

        /* get big graph */
        if ($big == '1') {
            if ($param == "ga:page-ses") {
                $results = $analytics->data_ga->get(
                    'ga:' . $id,
                    $startDate,
                    $endDate,
                    'ga:pageviews,ga:sessions',
                    array('dimensions' => 'ga:date')
                );
                $resp = [];
                foreach ($results->getRows() as $key => $row) {
                    $resp[$key] = array((date("Y-m-d", strtotime($row[0]))), ((round((int)$row[1] / (int)$row[2], 2))));
                }
            } else {
                $results = $analytics->data_ga->get(
                    'ga:' . $id,
                    $startDate,
                    $endDate,
                    $param,
                    array('dimensions' => 'ga:date')
                );
                $resp = [];
                foreach ($results->getRows() as $key => $row) {
                    $resp[$key] = array((date("Y-m-d", strtotime($row[0]))), ((int)$row[1]));
                }
            }
            /* get small graph */
        } else {
            if ($param == "ga:page-ses") {
                $results = $analytics->data_ga->get(
                    'ga:' . $id,
                    $startDate,
                    $endDate,
                    'ga:pageviews,ga:sessions',
                    array('dimensions' => 'ga:date')
                );

                $response = '';
                foreach ($results->getRows() as $key => $row) {
                    $response .= round((int)$row[1] / (int)$row[2], 2) . ',';
                }

                $resp = rtrim($response, ",");
            } else {
                $results = $analytics->data_ga->get(
                    'ga:' . $id,
                    $startDate,
                    $endDate,
                    $param,
                    array('dimensions' => 'ga:date')
                );
                $response = '';
                foreach ($results->getRows() as $row) {
                    $response .= (int)$row[1] . ',';
                }
                $resp = rtrim($response, ",");
            }
        }

        return $resp;
    }


    /**
     * Get Demography, System, Mobile
     * Call Ajax
     * @param id
     * @param startdate
     * @param enddate
     * @param metrics
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getLangbyPeriod()
    {

        $data = Input::all();
        $analytics = $this->analytics;
        if (strstr($data['param'], "-")) {
            $filt = explode("-", $data['param']);
            $results = $analytics->data_ga->get(
                'ga:' . $data['id'],
                $data['startDate'],
                $data['endDate'],
                'ga:sessions',
                array('dimensions' => $filt[0], "sort" => '-ga:sessions', "max-results" => '10', "filters" => "ga:deviceCategory==" . $filt[1])
            );
        } else {
            $results = $analytics->data_ga->get(
                'ga:' . $data['id'],
                $data['startDate'],
                $data['endDate'],
                'ga:sessions',
                array('dimensions' => $data['param'], "sort" => '-ga:sessions', "max-results" => '10')
            );
        }
        $response = [];

        $s = $results->getTotalsForAllResults();

        foreach ($results->getRows() as $key => $row) {
            $response[$key] = array(($row[0]), ((int)$row[1]), ((int)$s['ga:sessions']));
        }

        return view('googleanalytics::google/analytics.tabs', ['data' => $response]);
    }

    /**
     * Get all statistics for the selected period
     *
     * @param $analytics
     * @param $id
     * @param $startDate
     * @param $endDate
     * @return mixed $results
     */
    private function getGeneralStatistics($analytics, $id, $startDate, $endDate)
    {
        $results = $analytics->data_ga->get(
            'ga:' . $id,
            $startDate,
            $endDate,
            'ga:sessions,ga:avgSessionDuration,ga:users,ga:newUsers,ga:pageviews,ga:bounceRate,ga:percentNewSessions'
        );
        return str_replace('ga:', '', $results->getTotalsForAllResults());
    }

}
