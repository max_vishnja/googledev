<?php namespace MaxVishnja\GoogleAnalytics;

use Google_Client;
use Google_Service_Analytics;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;

class GoogleAnalyticsServiceProvider extends ServiceProvider
{

    /**
     *
     */
    public function boot(){
        $this->publishes([__DIR__ . '/../config/google.php' => config_path('google.php')], 'config');
        $this->publishes([__DIR__.'/../public' => public_path('packages/MaxVishnja/GoogleAnalytics'),], 'public');
        $this->loadViewsFrom(__DIR__ . '/../resources/views/', 'googleanalytics');
        require __DIR__ . '/Http/routes.php';
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/google.php', 'google');
        $app = $this->app;
        $this->app->bind('GoogleClient', function() {
            $gClient = new Google_Client();
            $gClient->setAccessToken(Session::get('token'));
            return $gClient;
        });

        $this->app->bind('analytics', function() use ($app) {

            $gClient = $this->app->make('GoogleClient');
            $analytics = new Google_Service_Analytics($gClient);
            return $analytics;
        });

    }


}