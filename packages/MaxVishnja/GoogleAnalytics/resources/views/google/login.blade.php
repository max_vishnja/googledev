@extends(config('google.views.layout'))

@section('content')
    <div class="row-fluid">
        <!-- block -->
        <div class="panel panel-default">
            <div class="navbar navbar-default ">
                <div class="panel-heading">Login to Google Aps</div>

            </div>

            <div class="panel-block">
                <div class="row-fluid login">
                    <div class="alert alert-warning text-center">
                        <h2>Authorize </h2>
                        <div>
                            <a class="btn btn-social btn-google btn-lg" href="{{ $loginUrl }}">
                                <span class="fa fa-google "></span> Sign in with Google
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /block -->
    </div>
@endsection
