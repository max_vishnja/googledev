@extends(config('google.views.layout'))

@section('content')
    <div class="row-fluid">
        <!-- block -->
        <div class="block">
            <div class="navbar navbar-inner block-header">
                <div class="muted pull-left">Enter</div>
                <div class="pull-right"><span class="badge badge-warning">View More</span>

                </div>
            </div>
            <div class="block-content collapse in">
                <div class="row page-header">
                    <h1 class="text-center"><a href="{{ route('ga.index') }}">Enter to Google Analytics</a></h1>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
@endsection
