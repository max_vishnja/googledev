<?php namespace MaxVishnja\GoogleAnalytics\Http\Controllers;

use App\Http\Controllers\Controller;
use Doctrine\Instantiator\Exception\InvalidArgumentException;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use MaxVishnja\GoogleAnalytics\Http\Services\GoogleLogin;

class GoogleAnalyticsController extends Controller {

    protected $google;


    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct(GoogleLogin $google)
    {
        $this->google = $google;
        $this->middleware('auth');
    }


    public function index()
    {
        if ($this->google->isLoggedIn()) {
            return redirect(route('ga.index'));
        }

        return redirect(route('google.login'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function login()
    {
        if ($this->google->isLoggedIn()) {
            return redirect(route('googleanalytics::google.index'));
        }
        $data['loginUrl'] = $this->google->getLoginUrl();
        return view('googleanalytics::google.login', $data);
    }

    /**
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws InvalidArgumentException
     */
    public function callback()
    {

        if (Input::has('error')) {
            dd(Input::get('error'));
        }

        if (Input::has('code')) {
            $code = Input::get('code');
            $this->google->login($code);
            return redirect(route('google.index'));
        } else {
            throw new InvalidArgumentException('The code attribute is missing');
        }
    }

    /**
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function logout()
    {
        Session::forget('token');
        return redirect(route('google.login'));
    }
}