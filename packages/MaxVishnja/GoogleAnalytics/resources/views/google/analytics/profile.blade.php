@extends(config('google.views.layout'))

@section('content')
    <div class="row-fluid">
        <!-- block -->
        <div class="panel panel-default">
            <div class="navbar navbar-default">
                <div class="col-md-6 col-sm-6 col-xs-12 text-left">
                    <h5>Profile <a href="{{ $profile->getWebsiteUrl() }}"
                                   target="_blank">{{  $profile->getWebsiteUrl() }}</a></h5>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-right">
                        <h5><span data-toggle="dropdown" class="label label-info droptoggle text-right">Date: <span
                                    class="st-d">{{$data['startDate']}}</span>
                            - <span class="en-d">{{$data['endDate']}}</span></span></h5>
                </div>
            </div>
            <div class="panel-body dropd">
                <div class="col-md-12 text-right">
                    {!! Form::open(array('route' => array('ga.statistic', $profile->getId()), 'id' => 'sendData', 'class'=>'form-inline')) !!}
                    <div class="form-group">
                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                        {!! Form::text('startDate', $data['SstartDate'], array('class' => 'form-control focused', 'id' => 'startDate', 'placeholder'=>'Start Date', 'required' => 'required')) !!}
                    </div>
                    <div class="form-group">
                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                        {!! Form::text('endDate', $data['EendDate'], array('class' => 'form-control focused','placeholder'=>'End Date', 'id' => 'endDate', 'required' => 'required')) !!}
                    </div>
                    {!! Form::submit('SHOW',array('class' => 'btn btn-primary btn-info')) !!}

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        <!-- /block -->
    </div>
    <div class="row-fluid section graph">
        <!-- block -->
        <div class="panel panel-default">
            <div class="navbar navbar-default">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h4><span class="label label-info headgraph">Session</span></h4>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 text-right but-period" role="group" aria-label="...">
                    <button class="btn btn-default day">Day</button>
                    <button class="btn btn-default week">Week</button>
                    <button class="btn btn-default month">Month</button>
                </div>
            </div>
            <div class="panel-body">
                <div class="col-md-12 col-sm-12 col-xs-12 text-center">

                    <div id="hero-graph" class="text-left"></div>
                    <div id="vmap"></div>
                    <button class="btn btn-info maps">See the geo map</button>
                </div>


            </div>
        </div>
        <!-- /block -->
    </div>
    @include("googleanalytics::google/analytics.stats")
    <div class="row-fluid section">
        <!-- block -->
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-4 demog" id="{{$profile->getId()}}">
                    <h5>Demographics</h5>
                    <ul class="nav nav-pills nav-stacked nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a class="statlang" id="ga:language"
                                                                  aria-controls="lang" role="tab" data-toggle="tab"
                            >Language</a></li>
                        <li role="presentation"><a class="statlang" id="ga:country" aria-controls="country"
                                                   role="tab" data-toggle="tab">Country</a></li>
                        <li role="presentation"><a class="statlang" id="ga:city" aria-controls="country" role="tab"
                                                   data-toggle="tab">City</a></li>

                        <h5>System</h5>

                        <li role="presentation"><a class="statlang" id="ga:browser-desktop"
                                                   aria-controls="lang" role="tab" data-toggle="tab">Browser</a>
                        </li>
                        <li role="presentation"><a class="statlang" id="ga:operatingSystem-desktop"
                                                   aria-controls="lang" role="tab" data-toggle="tab">Operating
                                system</a></li>
                        <li role="presentation"><a class="statlang" id="ga:networkLocation-desktop"
                                                   aria-controls="lang" role="tab" data-toggle="tab">Service
                                Provider</a></li>

                        <h5>Mobile</h5>

                        <li role="presentation"><a class="statlang" id="ga:browser-mobile"
                                                   aria-controls="lang" role="tab" data-toggle="tab">Operating
                                system</a></li>
                        <li role="presentation"><a class="statlang" id="ga:networkLocation-mobile"
                                                   aria-controls="lang" role="tab" data-toggle="tab">Service
                                Provider</a></li>
                        <li role="presentation"><a class="statlang" id="ga:browserSize-mobile"
                                                   aria-controls="lang" role="tab" data-toggle="tab">Screen
                                Resolution</a></li>
                    </ul>
                </div>

                <div class="col-md-8">
                    <div class="col-md-6 col-xs-6 demog">
                        <h6 class="namel">Language</h6>
                    </div>
                    <div class="col-md-3 col-xs-3 demog text-right">
                        <h6>Session</h6>
                    </div>
                    <div class="col-md-3 col-xs-3 demog">
                        <h6> % Session</h6>
                    </div>
                    <div class="tab-content viewsl">
                    </div>
                </div>
            </div>
        </div>
        <!-- /block -->
    </div>



@stop