<?php

return [
    'views' => [
        'layout' => 'googleanalytics::layouts.app-admin',
        'head' => 'googleanalytics::layouts.head',
        'footer' => 'googleanalytics::layouts.footer',
    ],
    'sections' => [
        'content' => 'content',
    ],
    'api' => [
        'app_name'          => 'Youtube', //Can be anything
        'client_id'         => '792765540974-ufafqnoo67va7v6g95k63ij8ib05p734.apps.googleusercontent.com',//can be found on the credentials page
        'client_secret'     => 'SRFrp3nVoNqGvepunxBBK-Hj',//on the credentials page
        'api_key'           => 'AIzaSyA7BALLYHsIY4Ch5qcxQa4qyAvSNTzoGe8'//can be found at the bottom of your credentials page
    ],
    'google_scopes' => [
        //'https://www.googleapis.com/auth/contacts.readonly',
//        'https://www.google.com/m8/feeds',
//        'https://www.googleapis.com/auth/calendar',
//        'https://www.googleapis.com/auth/plus.me',
//        'https://www.googleapis.com/auth/plus.login',
//        'https://www.googleapis.com/auth/plus.profile.emails.read',
//        'https://www.googleapis.com/auth/youtube',
//        'https://www.googleapis.com/auth/youtube.force-ssl',
//        'https://www.googleapis.com/auth/analytics',
        'https://www.googleapis.com/auth/analytics.edit',
        'https://www.googleapis.com/auth/analytics.readonly'
    ]
];