<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/googleanalytics';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }


    public function redirectToProvider($type)
    {
        if ($type == 'login') {
            return redirect('/googleanalytics');
        }

        $socialite = Socialite::driver($type);

        return $socialite->redirect();
    }


    public function handleProviderCallback($type)
    {
        try {
            $user = Socialite::driver($type)->user();
        } catch (\Exception $e) {
            return redirect('auth/' . $type);
        }

        $authUser = $this->findOrCreateUser($type, $user);
        Auth::login($authUser, true);

        return redirect('/');
    }


    private function findOrCreateUser($type, $user)
    {
        $authUser = User::where($type . '_id', $user->id)->first();

        if ($authUser) {
            return $authUser;
        }

        $user = User::create([
            'name' => $user->name,
            'email' => $user->email ? $user->email : $user->name . '@twitter.com',
            $type . '_id' => $user->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            $type . '_token' => $user->token,
            'processing' => 0
        ]);

        return $user;
    }
}
