<?php

Route::group(['prefix' => 'googleanalytics', 'middleware' => 'web', 'namespace' => 'MaxVishnja\GoogleAnalytics\Http\Controllers'], function () {


    Route::get('/', ['as' => 'google.index', 'uses' => 'GoogleAnalyticsController@index']);
    Route::get('/login', ['as' => 'google.login', 'uses' => 'GoogleAnalyticsController@login']);
    Route::get('/logout', ['as' => 'google.logout', 'uses' => 'GoogleAnalyticsController@logout']);
    Route::get('/callback', ['as' => 'google.callback', 'uses' => 'GoogleAnalyticsController@callback']);
    Route::get('/start', ['as' => 'ga.index', 'uses' => 'GoogleAnalyticsDataController@index']);
    Route::get('/account/{id}', ['as' => 'ga.account', 'uses' => 'GoogleAnalyticsDataController@getAccount']);
    Route::get('/account/{id}/webproperties/{idProperty}', ['as' => 'ga.property', 'uses' => 'GoogleAnalyticsDataController@getWebProperty']);
    Route::any('/account/statistic/{id}', ['as' => 'ga.statistic', 'uses' => 'GoogleAnalyticsDataController@getStatistic']);
    Route::post('/account/statlang', ['as' => 'ga.statlang', 'uses' => 'GoogleAnalyticsDataController@getLangbyPeriod']);
    Route::post('/account/biggraph', ['as' => 'ga.graph', 'uses' => 'GoogleAnalyticsDataController@getLittleGraph']);
    Route::post('/account/getmap', ['as' => 'ga.getmap', 'uses' => 'GoogleAnalyticsDataController@getMap']);
});

Route::group(['prefix' => 'googleanalytics', 'namespace' => 'MaxVishnja\GoogleAnalytics\Http\Controllers', 'middleware' => ['google', 'acl:module_google_analytics']], function () {

});
