<div class="row-fluid section">
    <!-- block -->
    <div class="panel panel-default">
        <div class="navbar navbar-default">
            <div class="panel-heading"><h4><span class="label label-info">All statistic</span></h4></div>
        </div>
        <div class="panel-body stat">
            <div class="col-md-10">
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">Sessions</span></div>
                    <div class="second" id="ga:sessions">
                        <h3 class="seconds sessions">{{$visit['ga:sessions']}}</h3>
                        <span class="line">{{$data['ga:sessions']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">PageViews</span></div>
                    <div class="second" id="ga:pageviews">
                        <h3 class="seconds pageviews">{{$visit['ga:pageviews']}}</h3>
                        <span class="line" >{{$data['ga:pageviews']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">Users</span></div>
                    <div class="second" id="ga:users">
                        <h3 class="seconds users">{{$visit['ga:users']}}</h3>
                        <span class="line">{{$data['ga:users']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">New Users</span></div>
                    <div class="second" id="ga:newUsers">
                        <h3 class="seconds sessionDuration">{{$visit['ga:newUsers']}}</h3>
                        <span class="line" >{{$data['ga:newUsers']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">Pages / Session</span>
                    </div>
                    <div class="second" id="ga:page-ses">
                        <h3 class="seconds pageses">{{round($visit['ga:pageviews']/$visit['ga:sessions'],2)}}</h3>
                        <span class="line">{{$data['ga:newUsers']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span
                                class="label label-info">Avg. Session Duration</span></div>
                    <div class="second" id="ga:avgSessionDuration">
                        <h3 class="seconds duration">
                            {{$visit['ga:avgSessionDuration']}}
                        </h3>
                        <span class="line">{{$data['ga:avgSessionDuration']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">% Bounce Rate</span></div>
                    <div class="second" id="ga:bounceRate">
                        <h3 class="seconds bounce">
                            {{round($visit['ga:bounceRate'],2)}}%
                        </h3>
                        <span class="line">{{$data['ga:bounceRate']}}</span>
                    </div>
                </div>
                <div class="col-md-3 text-center">
                    <div class="chart-bottom-heading"><span class="label label-info">% New Sessions</span></div>
                    <div class="second"  id="ga:percentNewSessions">
                        <h3 class="seconds newses">
                            {{round($visit['ga:percentNewSessions'],2)}}%
                        </h3>
                        <span class="line">{{$data['ga:percentNewSessions']}}</span>
                    </div>
                </div>
            </div>
            <div class="col-md-2 text-center">
                <div id="hero-donut" style="height: 250px;"></div>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>