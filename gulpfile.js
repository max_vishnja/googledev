var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.scripts([
            '/assets/moment.js',
            '/vendors/jquery-1.9.1.min.js',
            '/js/modernizr.min.js',
            '/js/date.js',
            '/js/jquery-ui.js',
            '/vendors/jquery.knob.js',
            '/bootstrap3/js/bootstrap.min.js',
            '/vendors/easypiechart/jquery.easy-pie-chart.js',
            '/vendors/raphael-min.js',
            '/vendors/morris/morris.min.js',
            '/assets/jquery.peity.min.js',
            '/assets/jquery.vmap.js',
            '/assets/jquery.vmap.world.js',
            '/assets/jquery.vmap.sampledata.js',
            '/assets/scripts.js'],
        'public/js/dependencies.js', 'node_modules')

        .scriptsIn("resources/js/", 'public/js/app.js')

        .styles([
            //"/bootstrap/css/bootstrap.min.css",
            "/bootstrap3/css/bootstrap.css",
           // "/bootstrap/css/bootstrap-responsive.min.css",
            "/vendors/easypiechart/jquery.easy-pie-chart.css",
            "/vendors/morris/morris.css",
            "/assets/styles.css",
            "/assets/query-ui.css",
            "/assets/jqvmap.css"
        ], 'public/css/style.css', 'node_modules')
});