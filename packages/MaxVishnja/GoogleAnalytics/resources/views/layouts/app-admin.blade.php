<!DOCTYPE html>
<html class="no-js">

<head>
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <link href="{!! asset('/css/style.css') !!}" rel="stylesheet" media="screen">
</head>
<body>
<div class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="btn btn-navbar navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="navbar-brand" href="/">My Google Analytics</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-expanded="false">
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout admin</a>
                            </li>
                            <li><a href="{{route('google.logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout
                                    Analytics</a></li>
                        </ul>
                    </li>
                @endif

            </ul>

        </div>
        <!--/.nav-collapse -->

    </div>
</div>
<div class="container-fluid">
    <div class="row-fluid">
        <div class="col-md-2 col-sm-3 nav-collapse collapse" id="sidebar">
            <ul class="nav nav-stacked bs-docs-sidenav  nav-pills">
                <li class="active">
                    <a class="dropmenu" href="{{route('google.index') }}"><i class="icon-chevron-right"></i> Click to
                        view accounts</a>
                    @if(isset($accounts))
                        <ul class="nav nav-list nav-collapse dr-dropmenu">
                            @foreach($accounts as $account)
                                <li class="dropdown">
                                    <a href="#" id="{{ $account->getId() }}" role="button" class="dropdown-toggle acc"
                                       data-toggle="dropdown"> {{ $account->getName() }}</a>
                                    <div class="infoac{{ $account->getId() }}">
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            </ul>
        </div>

        <div class="col-md-10 col-sm-9 home" id="content">
            @yield("content")
        </div>
    </div>
    <hr>
    <footer>
    </footer>
</div>
@extends(config('google.views.footer'))
