<div role="tabpanel" class="tab-pane active">
    @foreach ($data as $language)
        <div class="hov">
            <div class="col-md-6 col-xs-6">
                <p>{{$language[0]}}</p>
            </div>
            <div class="col-md-3 col-xs-3 text-right">
                <p>{{$language[1]}}</p>
            </div>
            <div class="col-md-3 col-xs-3">
                <p>{{round($language[1]/$language[2],4)*100}}%</p>
            </div>
        </div>
    @endforeach
</div>