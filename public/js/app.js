$(".knob").peity("line");
$(function () {
    /**
     * Ajax get Google Analytics Account
     **/

    $('.acc').on('click', function (e) {
        e.preventDefault();
        var id = $(this).attr('id');
        $.ajax({
            url: '/googleanalytics/account/' + id,
            success: function (data) {
                $('.infoac' + id).html(data);
            }

        });

    });

    /**
     * Slide graph and map
     **/
    $('.dropd').hide();
    $('#vmap').hide();
    $('.droptoggle').on('click', function (e) {
        e.preventDefault();
        $('.dropd').slideToggle('500');
    });
    var durationst = parseInt($('h3.duration').html());
    $('.duration').html(moment({minute: durationst / 60, seconds: durationst % 60}).format('mm:ss'));
    /**
     * Datepicker settings
     **/

    $('#startDate, #endDate').datepicker({
        useCurrent: false,
        dateFormat: 'yy-mm-dd',
        showTodayButton: true,
        showClose: true,
        allowInputToggle: true,
    });

    /**
     * Toogle account on left sidebar
     */

    $('.dr-dropmenu').hide();
    $('.dropmenu').on('click', function(e) {
        e.preventDefault();
        $('.dr-dropmenu').slideToggle('500');
        });

    /**
     * Get Statistic
     **/
    config = {
        xkey: '0',
        ykeys: ['1'],
        lineWidth: 2,
        hideHover: 'autos',
        fillOpacity: 0.6,
        behaveLikeLine: true,
        resize: true,
        lineColors: ["#81d5d9", "#a6e182"]
    }

    /**
     * Get Block Demography, System, Mobile
     **/
    $('.statlang').on('click', function (e) {
        e.preventDefault();
        $('.namel').html($(this).html());
        var id = $('.demog').attr('id');
        var param = $('.demog').attr('id');
        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=' + $(this).attr('id'),
            url: '/googleanalytics/account/statlang',
            beforeSend: preload('.viewsl'),
            success: function (data) {
                $('.viewsl').html(data);
            }

        });
    });

    /**
     * Get Statistic by period and show on statistic and world map
     **/

    $('#sendData').on('submit', function (e) {
        e.preventDefault();
        $('.sessions').parent().addClass('s-active');
        $('.but-period button').removeClass('active');
        $('.day').addClass('active');
        $('#vmap').slideUp('500');
        var stdate = new Date($('#startDate').val()).toString('MMM d, yyyy');
        var endate = new Date($('#endDate').val()).toString('MMM d, yyyy');
        $('span.st-d').html(stdate);
        $('span.en-d').html(endate);
        $('#hero-graph').html('');
        $('#vmap').html('');
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: $(this).serialize(),
            dataType: 'json',
            beforeSend: preload('#hero-graph'),
            success: function (data) {
                var users = data.generalStatistics['ga:users'],
                    newUsers = data.generalStatistics['ga:newUsers'],
                    sessions = data.generalStatistics['ga:sessions'],
                    bounce = Number(data.generalStatistics['ga:bounceRate']),
                    newses = Number(data.generalStatistics['ga:percentNewSessions']),
                    duration = parseInt(data.generalStatistics['ga:avgSessionDuration']),
                    pageviews = data.generalStatistics['ga:pageviews'];
                var pageses = pageviews / sessions;
                $('.sessions').html(sessions);
                $('.pageviews').html(pageviews);
                $('.sessionDuration').html(newUsers);
                $('.users').html(users);
                $('.duration').html(moment({minute: duration / 60, seconds: duration % 60}).format('mm:ss'));
                $('.pageses').html(pageses.toFixed(2));
                $('.bounce').html(bounce.toFixed(2) + "%");
                $('.newses').html(newses.toFixed(2) + "%");
                $('.dropd').slideUp('500');
                $('.headgraph').html("Sessions");
                var graph = data.visitByDay;
                config.element = 'hero-graph';
                config.data = graph;
                config.labels = ['Sessions'];
                Morris.Area(config);
                Morris.Donut({
                    element: 'hero-donut',
                    data: [
                        {label: 'New visitors', value: Number(newses).toFixed(1)},
                        {label: 'Returning visitors', value: (100 - newses).toFixed(1)}
                    ],
                    colors: ["#30a1ec", "#8ac368"],
                    formatter: function (y) {
                        return y + "%"
                    }
                });
                $("#hero-graph img").hide();
                $('.active .statlang').click();
                $('.chart-bottom-heading').click();
            }
        });
        var id = $('.demog').attr('id');
        var val = "Sessions";
        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=ga:sessions',
            dataType: 'json',
            url: '/googleanalytics/account/getmap',
            success: function (data) {
                generateMap(data, val);
            }

        });
    });

    /**
    * Click button Week
     **/

    $('.week').on('click', function(e) {
        e.preventDefault();
        $('.but-period button').removeClass('active');
        $(this).addClass('active');
        $('#hero-graph').html('');
        var zag = $('.s-active').prev().find('.label').html();
        var id = $('.demog').attr('id');
        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=' + $('.s-active').attr('id') + '&big=1',
            dataType: 'json',
            url: '/googleanalytics/account/biggraph',
            beforeSend: preload('#hero-graph'),
            success: function (data) {
                var dayseven=parseInt(data.length/7);
                var newData=[];
                var i=1;
                while(i<=dayseven) {
                    var s=0;
                    for(var j=(i-1)*7; j<i*7; j++){
                    s=s+data[j][1];
                    }
                    newData[i-1]=[data[i*7][0],s];
                    i++;
                }
                var graph2 = newData;
               // var graph2 = data;
                config.element = 'hero-graph';
                config.data = graph2;
                config.labels = [zag];
                Morris.Area(config);
                $("#hero-graph img").hide();
            }

        });

    });

    /**
     * Click button Month
     **/
    $('.month').on('click', function(e) {
        e.preventDefault();
        $('.but-period button').removeClass('active');
        $(this).addClass('active');
        $('#hero-graph').html('');
        var zag = $('.s-active').prev().find('.label').html();
        var id = $('.demog').attr('id');
        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=' + $('.s-active').attr('id') + '&big=1',
            dataType: 'json',
            url: '/googleanalytics/account/biggraph',
            beforeSend: preload('#hero-graph'),
            success: function (data) {
                var dayseven=parseInt(data.length/30);
                var newData=[];
                var i=1;
                while(i<=dayseven) {
                    var s=0;
                    for(var j=(i-1)*30; j<i*30; j++){
                        s=s+data[j][1];
                    }
                    newData[i-1]=[data[i*29][0],s];
                    i++;
                }
                var graph = newData;
                config.element = 'hero-graph';
                config.data = graph;
                config.labels = ['Sessions'];
                Morris.Area(config);
                $("#hero-graph img").hide();

            }
        });
    });

    /**
     * Click button Day
     **/
    $('.day').on('click', function(e) {
        e.preventDefault();
        $('.but-period button').removeClass('active');
        $(this).addClass('active');
        $('.s-active').click();
    });


    /**
     * Show world map
     **/
    $('.maps').on('click', function (e) {
        e.preventDefault();
        $('#vmap').slideToggle('500');
    });

    /**
     * Get Big graph and world map on click small graph
     **/
    $('.second').on("click", function (e) {
        e.preventDefault();
        $('.but-period button').removeClass('active');
        $('.day').addClass('active');
        $('div.s-active').removeClass('s-active');
        $(this).addClass('s-active');
        $('#vmap').slideUp('500');
        $('#vmap').html('');
        var zag = $(this).prev().find('.label').html();
        $('.headgraph').html(zag);
        $("html, body").animate({ scrollTop: 0 }, 600);
        var id = $('.demog').attr('id');
        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=' + $(this).attr('id') + '&big=1',
            dataType: 'json',
            url: '/googleanalytics/account/biggraph',
            beforeSend: preload('#hero-graph'),
            success: function (data) {
                var graph2 = data;
                config.element = 'hero-graph';
                config.data = graph2;
                config.labels = [zag];
                Morris.Area(config);
                $("#hero-graph img").hide();
            }

        });
        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=' + $(this).attr('id'),
            dataType: 'json',
            url: '/googleanalytics/account/getmap',
            success: function (data) {
                generateMap(data, zag);
            }

        });
    });

    /**
     * Get Small graph
     **/

    $('.chart-bottom-heading').on('click', function (e) {
        e.preventDefault();
        var id = $('.demog').attr('id');

        $.ajax({
            type: 'POST',
            data: $("#sendData").serialize() + '&id=' + id + '&param=' + $(this).next().attr('id') + '&big=0',
            url: '/googleanalytics/account/biggraph',
            success: function (data) {
                $(this).next().find('.line').html(data);
                $(".line").peity("line");
            }.bind(this)

        });
    });

    /**
     * Load Ajax function on start
     **/
    var urll = $('#sendData').attr('action');
    if (urll) {
        $('#sendData').submit();
        $('.active .statlang').click();

    }
    /**
     * Preloader
     **/
    function preload(obj) {
        $(obj).html('<img class="center" src="/public/images/722.gif">');
    }

    /**
     * Config small graph
     **/
    $.fn.peity.defaults.line = {
        delimiter: ",",
        fill: "#c6d9fd",
        height: 16,
        max: null,
        min: 0,
        stroke: "#4d89f9",
        strokeWidth: 1,
        width: 145
    }
    $(".line").peity("line");
});

/**
 * Generate world map
 **/
function generateMap(gdpData, val) {

    var max = 0,
        min = Number.MAX_VALUE,
        cc,
        startColor = [78, 93, 100],
        endColor = [0, 39, 57],
        colors = {},
        hex;

//find maximum and minimum values
    for (cc in gdpData) {
        if (parseFloat(gdpData[cc]) > max) {
            max = parseFloat(gdpData[cc]);
        }
        if (parseFloat(gdpData[cc]) < min) {
            min = parseFloat(gdpData[cc]);
        }
    }

//set colors according to values of GDP
    for (cc in gdpData) {
        if (gdpData[cc] > 0) {
            colors[cc] = '#';
            for (var i = 0; i < 3; i++) {
                hex = Math.round(startColor[i]
                    + (endColor[i]
                    - startColor[i])
                    * (gdpData[cc] / (max - min))).toString(16);

                if (hex.length == 1) {
                    hex = '0' + hex;
                }

                colors[cc] += (hex.length == 1 ? '0' : '') + hex;
            }
        }
    }
    jQuery('#vmap').vectorMap({
        map: 'world_en',
        backgroundColor: null,
        hoverColor: false,
        colors: colors,
        hoverOpacity: 0.7,
        selectedColor: '#666666',
        enableZoom: false,
        showTooltip: true,
        values: gdpData,
        selectedRegion: null,
        onLabelShow: function (event, label, code) {
            if (gdpData[code] > 0)
                label.append('<br/> ' + gdpData[code] + ' ' + val);
        },
        normalizeFunction: 'polynomial'
    });
}





//# sourceMappingURL=app.js.map
