@if(isset($webProperties))
    @foreach($webProperties as $property)
        <li class="well well-sm web-property text-right">
            <a class="webp" id="{{$property->getAccountId()}}-{{$property->getId()}}" href="{{ route('ga.property', [$property->getAccountId(), $property->getId()]) }}">{{ $property->getName() }}</a>
        </li>
    @endforeach
@endif

